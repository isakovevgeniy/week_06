package ru.edu;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.edu.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class StudentsRepositoryCRUDTest {
    StudentsRepositoryCRUDimpl app;
    Connection getH2Connection;
    Statement statement;
    String tableName;

    Student studentMock = Mockito.mock(Student.class);
    Student anotherStudentMock = Mockito.mock(Student.class);
    Date birthDate = new Date(94, 01, 20);
    UUID studentMockUUID;
    UUID anotherStudentMockUUID;

    StudentsRepositoryCRUDimpl appSpy;

    @Before
    public void setUp() throws Exception {
        String url = "jdbc:h2:./file/database";
        getH2Connection = DriverManager.getConnection(url, "sa", "");
        statement = getH2Connection.createStatement();
        tableName = "Students";

//        SqlUtil sqlUtil = new SqlUtil(getH2Connection, statement);
//        sqlUtil.dropTable();
//        sqlUtil.createNewTable();

        app = new StudentsRepositoryCRUDimpl(getH2Connection);

        studentMockUUID = UUID.randomUUID();
        Mockito.when(studentMock.getId()).thenReturn(null);
        Mockito.when(studentMock.getFirstName()).thenReturn("Alex");
        Mockito.when(studentMock.getLastName()).thenReturn("Vasilyev");
        Mockito.when(studentMock.getBirthDate()).thenReturn(birthDate);
        Mockito.when(studentMock.isGraduated()).thenReturn(true);
        Mockito.when(studentMock.toString()).thenReturn("Student{firstName='Alex', lastName='Vasilyev', birthDate=1994-02-20}");

        anotherStudentMockUUID = UUID.randomUUID();
        Mockito.when(anotherStudentMock.getId()).thenReturn(anotherStudentMockUUID);
        Mockito.when(anotherStudentMock.getFirstName()).thenReturn("Ivan");
        Mockito.when(anotherStudentMock.getLastName()).thenReturn("Ivanov");
        Mockito.when(anotherStudentMock.getBirthDate()).thenReturn(birthDate);
        Mockito.when(anotherStudentMock.isGraduated()).thenReturn(true);
        Mockito.when(anotherStudentMock.toString()).thenReturn("Student{firstName='Ivan', lastName='Ivanov', birthDate=1994-02-20}");
    }

    @After
    public void tearDown() throws Exception {
        statement.close();
        getH2Connection.close();
    }

    @Test
    public void createTest() throws SQLException {
        UUID uuid = app.create(studentMock);
        assertNotNull(uuid);
        ResultSet resultSet = statement.executeQuery("SELECT * FROM Students WHERE id = '" + uuid + "'");
        assertTrue(resultSet.next());
        assertEquals("Alex", resultSet.getString("firstName"));
        assertEquals(birthDate, resultSet.getDate("birthDate"));
        assertTrue(resultSet.getBoolean("isGraduated"));
        resultSet.close();
    }

    @Test
    public void createIfStudentExistsTest(){
        appSpy = Mockito.spy(app);
        UUID uuid = appSpy.create(studentMock);
        Mockito.when(studentMock.getId()).thenReturn(uuid);
        appSpy.create(studentMock);
        Mockito.verify(appSpy, Mockito.times(1)).update(studentMock);
    }

    @Test
    public void createWithNullUUIDTest(){
        appSpy = Mockito.spy(app);
        Mockito.when(studentMock.getId()).thenReturn(null);
        appSpy.create(studentMock);
    }

    @Test
    public void selectByIdTestWhenIdIsAbsent() {
        UUID uuid = UUID.nameUUIDFromBytes("Student{firstName='Egor', lastName='Egorov', birthDate=1994-01-20}".getBytes());
        assertNull(app.selectById(uuid));
    }

    @Test
    public void selectByIdTest() {
        UUID uuid = app.create(studentMock);
        assertEquals(studentMock.toString(), app.selectById(uuid).toString());
    }

    @Test
    public void selectAllTest() {
        app.create(studentMock);
        app.create(anotherStudentMock);
        assertEquals(2, app.selectAll().size());
        assertEquals(studentMock.toString(), app.selectAll().get(0).toString());
        assertEquals(anotherStudentMock.toString(), app.selectAll().get(1).toString());
    }

    @Test
    public void updateTest() throws SQLException {
        UUID uuid = app.create(anotherStudentMock);
        ResultSet resultSet = statement.executeQuery("SELECT isGraduated FROM Students WHERE id = '" + uuid + "'");
        assertTrue(resultSet.next());
        assertTrue(resultSet.getBoolean(1));
        resultSet.close();

        Mockito.when(anotherStudentMock.isGraduated()).thenReturn(false);
        assertEquals(1, app.update(anotherStudentMock)); //количество обновленных записей
        ResultSet resultSet2 = statement.executeQuery("SELECT isGraduated FROM Students WHERE id = '" + uuid + "'");
        assertTrue(resultSet2.next());
        assertFalse(resultSet2.getBoolean(1)); //результат изменения колонки isGraduated
        resultSet2.close();
    }

    @Test
    public void removeTest() {
        List<UUID> idList = new ArrayList<>();
        idList.add(app.create(studentMock));
        idList.add(app.create(anotherStudentMock));

        assertEquals(2, app.selectAll().size());

        assertEquals(2, app.remove(idList));
        assertEquals(0, app.selectAll().size());
    }
}