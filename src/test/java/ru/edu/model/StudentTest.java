package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;


import java.sql.Date;
import java.util.UUID;

public class StudentTest {
    UUID uuid = UUID.randomUUID();
    Date birthDate = new Date(94, 0, 20);
    Student student = new Student(uuid,"Alex", "Vasilyev", birthDate, true);


    @Test
    public void getIdTest() {
        Assert.assertEquals(uuid, student.getId());
    }

    @Test
    public void setIdTest() {
        UUID newUUID = UUID.randomUUID();
        student.setId(newUUID);
        Assert.assertEquals(newUUID, student.getId());
    }

    @Test
    public void getFirstNameTest() {
        Assert.assertEquals("Alex", student.getFirstName());
    }

    @Test
    public void setFirstNameTest() {
        student.setFirstName("NewName");
        Assert.assertEquals("NewName", student.getFirstName());
    }

    @Test
    public void getLastNameTest() {
        Assert.assertEquals("Vasilyev", student.getLastName());
    }

    @Test
    public void setLastNameTest() {
        student.setLastName("NewLastName");
        Assert.assertEquals("NewLastName", student.getLastName());
    }

    @Test
    public void getBirthDateTest() {
        Assert.assertEquals(birthDate, student.getBirthDate());
    }

    @Test
    public void setBirthDateTest() {
        student.setBirthDate(new Date(90, 5, 1));
        Assert.assertEquals(new Date(90, 5, 1), student.getBirthDate());
    }

    @Test
    public void isGraduatedTest() {
        Assert.assertTrue(student.isGraduated());
    }

    @Test
    public void setGraduatedTest() {
        student.setGraduated(false);
        Assert.assertFalse(student.isGraduated());
    }

    @Test
    public void toStringTest() {
        String line = "Student{firstName='Alex', lastName='Vasilyev', birthDate=1994-01-20}";
        Assert.assertEquals(line, student.toString());
    }
}

