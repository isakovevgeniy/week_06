package ru.edu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class SqlUtil {
    /**
     * Имя таблицы.
     */
    private String tableName = "Students";
    /**
     * Коннекшн.
     */
    private Connection connection;
    /**
     * Стейтмент.
     */
    private Statement statement;

    /**
     * Конструктор поумолчанию.
     */
    public SqlUtil() {
        final String dataBaseName = "Students";
        final String jdbcUrl =
                "jdbc:postgresql://localhost/" + dataBaseName;
        final Properties properties = new Properties();
        properties.setProperty("user", "postgres");
        properties.setProperty("password", "123");

        try {
            connection = DriverManager.getConnection(jdbcUrl, properties);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.getSQLState();
            e.printStackTrace();
        }
    }

    public SqlUtil(final Connection pConnection, Statement pStatement) {
        connection = pConnection;
        statement = pStatement;
    }
    /**
     * Создание новой таблициы типа Students.
     */
    public void createNewTable() {
        try {
            statement.execute(" CREATE TABLE " + tableName + " ( "
                    + " id varchar(64) PRIMARY KEY , "
                    + " firstName varchar(32), "
                    + " lastName varchar(32), "
                    + " birthDate date, "
                    + " isGraduated boolean )");
        } catch (SQLException e) {
            e.getSQLState();
            e.printStackTrace();
        }
    }

    /**
     * Удаление таблицы.
     * @throws SQLException exception
     */
    public void dropTable() {
        try {
            statement.executeUpdate("DROP TABLE " + tableName);
        } catch (SQLException e) {
            e.getSQLState();
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SqlUtil sqlUtil = new SqlUtil();
        sqlUtil.dropTable();
        sqlUtil.createNewTable();
    }
}
