package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StudentsRepositoryCRUDimpl implements StudentsRepositoryCRUD {
    /**
     * prepared statement.
     */
    PreparedStatement statement;

    /**
     * resultSet.
     */
    ResultSet resultSet;

    /**
     * insert.
     */
    private static final String INSERT = "INSERT INTO Students"
            + "(id, firstName, lastName, birthDate, isGraduated)"
            + "VALUES (?, ?, ?, ?, ?)";
    /**
     *  select to check.
     */
    private static final String SELECT_TO_CHECK = "SELECT id FROM Students"
            + " WHERE firstName = ? AND lastName = ? AND birthDate = ?";
    /**
     * select by id.
     */
    private static final String SELECT_BY_ID = "SELECT * FROM Students"
            + " WHERE id = ?";
    /**
     * update.
     */
    private static final String UPDATE = "UPDATE Students SET "
            + "isGraduated = ? "
            + "WHERE id = ? ";

    /**
     * Коннекшн.
     */
    private Connection connection;

    /**
     * Другой конструктор.
     *
     * @param pConnection connection
     */
    public StudentsRepositoryCRUDimpl(final Connection pConnection) {
        connection = pConnection;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        UUID uuid =  student.getId() == null
                ? UUID.randomUUID() : student.getId();
        if (isStudentExists(student)) {
            update(student);
        } else {
            executeUpdate(INSERT, uuid,
                    student.getFirstName(),
                    student.getLastName(),
                    new java.sql.Date(student.getBirthDate().getTime()),
                    student.isGraduated());
        }
        return uuid;
    }

    /**
     * Проверяет наличие студентов в таблице по ID.
     *
     * @param student student
     * @return - boolean
     */
    private boolean isStudentExists(final Student student) {
        try {
        resultSet = executeSelect(SELECT_TO_CHECK,
                    student.getFirstName(),
                    student.getLastName(),
                    new java.sql.Date(student.getBirthDate().getTime()));
            return resultSet == null ? false : resultSet.next();
        } catch (SQLException e) {
            throw new RuntimeException("isStudentExists failed " + e);
        }
    }

    /**
     * Обновление записи в БД.
     *
     * @param student - студент
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {
        return executeUpdate(UPDATE, student.isGraduated(), student.getId());
    }

    /**
     * execute select.
     * @param query query
     * @param values values
     * @return return
     */
    private ResultSet executeSelect(final String query, final Object... values) {
        try {
            statement = connection.prepareStatement(query);
            if (values != null && values.length != 0) {
                for (int i = 0; i < values.length; i++) {
                    statement.setObject(i + 1, values[i]);
                }
            }
            System.out.println(query);
            return statement.execute() ? statement.getResultSet() : null;
        } catch (SQLException e) {
            throw new RuntimeException("execute failed " + e);
        }
    }

    /**
     * execute update.
     * @param query query
     * @param values values
     * @return return
     */
    private int executeUpdate(final String query, final Object... values) {
        try {statement = connection.prepareStatement(query);
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            System.out.println(query);
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("execute failed " + e);
        }
    }

    /**
     * Получение записи по uuid из БД.
     *
     * @param uuid - ID
     * @return - Студент из таблицы
     */
    @Override
    public Student selectById(final UUID uuid) {
        resultSet = executeSelect(SELECT_BY_ID, uuid);
        try {
            return createStudentFromResultSet().get(0);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("id not found");
            return null;
        }
    }

    /**
     * Получает список студентов из ResultSet.
     *
     * @return списко студентов List<Student>
     */
    private List<Student> createStudentFromResultSet() {
        List<Student> students = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Student student = new Student();
                student.setId(UUID.fromString(resultSet.getString("id")));
                student.setFirstName(resultSet.getString("firstName"));
                student.setLastName(resultSet.getString("lastName"));
                student.setBirthDate(resultSet.getDate("birthDate"));
                student.setGraduated(resultSet.getBoolean("isGraduated"));

                students.add(student);
            }
        } catch (SQLException e) {
            throw new RuntimeException("createStudentFromResultSet failed " + e);
        }
        return students;
    }

    /**
     * Получение всех записей из БД.
     *
     * @return - список студентов в таблице List<Student>
     */
    @Override
    public List<Student> selectAll() {
        resultSet = executeSelect("SELECT * FROM Students");
        return createStudentFromResultSet();
    }


    /**
     * Удаление указанных записей по id.
     *
     * @param idList - список ID
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {
        return executeUpdate(createRemoveSQLQuery(idList), idList.toArray());
    }

    /**
     * createRemoveSQLQuery.
     * @param idList id list
     * @return sql query
     */
    private String createRemoveSQLQuery(List<UUID> idList) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("DELETE FROM Students WHERE id IN (");
        for (int i = 0; i < idList.size(); i++) {
            if (i != 0) {
                sqlQuery.append(",");
            }
            sqlQuery.append(" ? ");
        }
        sqlQuery.append(");");
        System.out.println(sqlQuery);
        return sqlQuery.toString();
    }
}
