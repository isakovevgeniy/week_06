package ru.edu.model;

import java.util.Date;
import java.util.UUID;

/**
 * Класс отражающий структуру хранимых в таблице полей.
 */
public class Student {
    /**
     * просто конструктор.
     */
    public Student() {
    }

    /**
     * constructor.
     * @param pid id
     * @param pfirstName fn
     * @param plastName ln
     * @param pbirthDate bd
     * @param pisGraduated ig
     */
    public Student(final UUID pid,
                   final String pfirstName,
                   final String plastName,
                   final Date pbirthDate,
                   final boolean pisGraduated) {
        id = pid;
        firstName = pfirstName;
        lastName = plastName;
        birthDate = pbirthDate;
        isGraduated = pisGraduated;
    }

    /**
     * get uuid.
     * @return uuid
     */
    public UUID getId() {
        return id;
    }

    /**
     * setter.
     * @param pid id
     */
    public void setId(final UUID pid) {
        this.id = pid;
    }

    /**
     * getter.
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter.
     * @param pFirstName firstName
     */
    public void setFirstName(final String pFirstName) {
        firstName = pFirstName;
    }

    /**
     * Getter.
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * setter.
     * @param plastName lastName
     */
    public void setLastName(final String plastName) {
        this.lastName = plastName;
    }

    /**
     * getter BirthDate.
     * @return birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * setter.
     * @param pbirthDate birthDate
     */
    public void setBirthDate(final Date pbirthDate) {
        this.birthDate = pbirthDate;
    }

    /**
     * getter.
     * @return boolean
     */
    public boolean isGraduated() {
        return isGraduated;
    }

    /**
     * setter.
     * @param pgraduated boolean
     */
    public void setGraduated(final boolean pgraduated) {
        isGraduated = pgraduated;
    }

    /**
     * Первичный ключ.
     *
     * Рекомендуется генерировать его только внутри
     * StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект не будет сохранен в БД,
     * он не должен иметь значение id.
     */
    private UUID id;
    /**
     * firstName.
     */
    private String firstName;
    /**
     * LastName.
     */
    private String lastName;
    /**
     * birthDate.
     */
    private Date birthDate;
    /**
     * is Graduated.
     */
    private boolean isGraduated;

    /**
     * toString.
     * @return string
     */
    @Override
    public String toString() {
        return "Student{"
                + "firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", birthDate=" + birthDate
                + '}';
    }
}
